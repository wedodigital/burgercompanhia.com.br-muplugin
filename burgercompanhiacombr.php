<?php
/**
 * Burger Companhia - mu-plugin
 *
 * PHP version 7
 *
 * @category  Wordpress_Mu-plugin
 * @package   BurgerCompanhia
 * @author    Cimbre <contato@cimbre.com.br>
 * @copyright 2019 Cimbre
 * @license   Proprietary https://cimbre.com.br
 * @link      https://burgercompanhia.com.br
 *
 * @wordpress-plugin
 * Plugin Name: Burger Companhia - mu-plugin
 * Plugin URI:  https://burgercompanhia.com.br
 * Description: Customizations for burgercompanhia.com.br site
 * Version:     1.0.1
 * Author:      Cimbre
 * Author URI:  https://cimbre.com.br/
 * Text Domain: burgercompanhia
 * License:     Proprietary
 * License URI: https://cimbre.com.br
 */

// If this file is called directly, abort.
if (!defined('WPINC')) {
    die;
}

/**
 * Load Translation
 *
 * @return void
 */
add_action(
    'plugins_loaded',
    function () {
        load_muplugin_textdomain('burgercompanhiacombr', basename(dirname(__FILE__)).'/languages');
    }
);

/**
 * Hide editor from all pages
 */
add_action(
    'admin_init',
    function () {
        remove_post_type_support('page', 'editor');
    }
);

/***********************************************************************************
 * Callback Functions
 **********************************************************************************/

 /**
  * Metabox for Page Slug
  *
  * @param bool  $display  Display
  * @param array $meta_box Metabox 
  *
  * @return bool $display
  *
  * @author Tom Morton
  * @link   https://github.com/CMB2/CMB2/wiki/Adding-your-own-show_on-filters
  */
function Cmb2_Metabox_Show_On_slug($display, $meta_box)
{
    if (!isset($meta_box['show_on']['key'], $meta_box['show_on']['value'])) {
        return $display;
    }

    if ('slug' !== $meta_box['show_on']['key']) {
        return $display;
    }

    $post_id = 0;

    // If we're showing it based on ID, get the current ID
    if (isset($_GET['post'])) {
        $post_id = $_GET['post'];
    } elseif (isset($_POST['post_ID'])) {
        $post_id = $_POST['post_ID'];
    }

    if (!$post_id) {
        return $display;
    }

    $slug = get_post($post_id)->post_name;

    // See if there's a match
    return in_array($slug, (array) $meta_box['show_on']['value']);
}
add_filter('cmb2_show_on', 'Cmb2_Metabox_Show_On_slug', 10, 2);

/**
 * Gets a number of terms and displays them as options
 *
 * @param CMB2_Field $field CMB2 Field
 *
 * @return array An array of options that matches the CMB2 options array
 */
function Cmb2_getTermOptions($field)
{
    $args = $field->args('get_terms_args');
    $args = is_array($args) ? $args : array();

    $args = wp_parse_args($args, array('taxonomy' => 'category'));

    $taxonomy = $args['taxonomy'];

    $terms = (array) cmb2_utils()->wp_at_least('4.5.0')
        ? get_terms($args)
        : get_terms($taxonomy, $args);

    // Initate an empty array
    $term_options = array();
    if (!empty($terms)) {
        foreach ($terms as $term) {
            $term_options[ $term->term_id ] = $term->name;
        }
    }
    return $term_options;
}

/***********************************************************************************
 * Pages Custom Fields
 * ********************************************************************************/

/**
 * Front-page
 */
add_action(
    'cmb2_admin_init',
    function () {
        // Start with an underscore to hide fields from custom fields list
        $prefix = '_burgercompanhiacombr_frontpage_';

        /**
         * Hero
         */
        $cmb_hero = new_cmb2_box(
            array(
                'id'            => 'burgercompanhiacombr_frontpage_hero_id',
                'title'         => __('Hero', 'burgercompanhiacombr'),
                'object_types'  => array('page'), // post type
                'show_on' => array('key' => 'slug', 'value' => 'front-page'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );
                
        //Hero Group
        $hero_id = $cmb_hero->add_field(
            array(
                'id'          => $prefix . 'hero_slides',
                'type'        => 'group',
                'description' => '',
                'options'     => array(
                    'group_title'   =>__('Slide {#}', 'burgercompanhiacombr'),
                    'add_button'   =>__('Add Another Slide', 'burgercompanhiacombr'),
                    'remove_button' =>__('Remove Slide', 'burgercompanhiacombr'),
                    'sortable'      => true, // beta
                ),
            )
        );

        //Background Color
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Background Color', 'burgercompanhiacombr'),
                'desc'       => '',
                'id'         => 'bkg_color',
                'type'       => 'colorpicker',
                'default'    => '#ffffff',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'palettes' => array('#ffffff', '#000000', '#ffb500', '#980500'),
                        ),
                    ),
                ),
            )
        );

        //Hero Image
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Background Image (Landscape)', 'burgercompanhiacombr'),
                'description' => '',
                'id'          => 'bkg_image_landscape',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'burgercompanhiacombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );

        //Hero Image
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Background Image (Portrait)', 'burgercompanhiacombr'),
                'description' => '',
                'id'          => 'bkg_image_portrait',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'burgercompanhiacombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );

        //Hero Title
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Title', 'burgercompanhiacombr'),
                'desc'       => '',
                'id'         => 'title',
                'type'       => 'textarea_code',
                'default'    => 'Blog',
            )
        );

        /**
         * Address
         */
        $cmb_address = new_cmb2_box(
            array(
                'id'            => 'burgercompanhiacombr_frontpage_address_id',
                'title'         => __('Address', 'burgercompanhiacombr'),
                'object_types'  => array('page'), // post type
                'show_on' => array('key' => 'slug', 'value' => 'front-page'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        $cmb_address->add_field(
            array(
                'name'       => __('Address', 'burgercompanhiacombr'),
                'desc'       => '',
                'id'         => $prefix . 'address',
                'type'       => 'textarea_code',
            )
        );

        /**
         * Orders
         */
        $cmb_orders = new_cmb2_box(
            array(
                'id'            => 'burgercompanhiacombr_frontpage_order_id',
                'title'         => __('Orders', 'burgercompanhiacombr'),
                'object_types'  => array('page'), // post type
                'show_on' => array('key' => 'slug', 'value' => 'front-page'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Orders Title
        $cmb_orders->add_field(
            array(
                'name'       => __('Title', 'burgercompanhiacombr'),
                'desc'       => '',
                'id'         => $prefix . 'orders_title',
                'type'       => 'text',
            )
        );

        //Orders Phone
        $cmb_orders->add_field(
            array(
                'name'       => __('Phone Text', 'burgercompanhiacombr'),
                'desc'       => '',
                'id'         => $prefix . 'orders_phone_text',
                'type'       => 'textarea_code',
            )
        );

        //Orders Phone Number
        $cmb_orders->add_field(
            array(
                'name'       => __('Phone Number', 'burgercompanhiacombr'),
                'desc'       => '',
                'id'         => $prefix . 'orders_phone_number',
                'type'       => 'text',
            )
        );

        //Orders WhatsApp
        $cmb_orders->add_field(
            array(
                'name'       => __('WhatsApp Text', 'burgercompanhiacombr'),
                'desc'       => '',
                'id'         => $prefix . 'orders_whatsapp_text',
                'type'       => 'textarea_code',
            )
        );

        //Orders WhatsApp URL
        $cmb_orders->add_field(
            array(
                'name'       => __('WhatsApp URL', 'burgercompanhiacombr'),
                'desc'       => '',
                'id'         => $prefix . 'orders_whatsapp_url',
                'type'       => 'text_url',
            )
        );
        
        //Menu Btn Text
        $cmb_orders->add_field(
            array(
                'name'       => __('Menu Button Text', 'burgercompanhiacombr'),
                'desc'       => '',
                'id'         => $prefix . 'orders_menu_btn_text',
                'type'       => 'textarea_code',
            )
        );
        
        //Menu File
        $cmb_orders->add_field(
            array(
                'name'    => __('Menu File', 'burgercompanhiacombr'),
                'desc'    => __('Upload PDF file', 'burgercompanhiacombr'),
                'id'      => $prefix . 'orders_menu_file',
                'type'    => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' => 'Add File' // Change upload button text. Default: "Add or Upload File"
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => 'application/pdf', // Make library only display PDFs.
                    // Or only allow gif, jpg, or png images
                    // 'type' => array(
                    // 	'image/gif',
                    // 	'image/jpeg',
                    // 	'image/png',
                    // ),
                ),
                'preview_size' => 'medium', // Image size to use when previewing in the admin.
            )
        );

        /**
         * Social
         */
        $cmb_social = new_cmb2_box(
            array(
                'id'            => 'burgercompanhiacombr_frontpage_social_id',
                'title'         => __('Social', 'burgercompanhiacombr'),
                'object_types'  => array('page'), // post type
                'show_on' => array('key' => 'slug', 'value' => 'front-page'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Orders Title
        $cmb_social->add_field(
            array(
                'name'       => __('Title', 'burgercompanhiacombr'),
                'desc'       => '',
                'id'         => $prefix . 'social_title',
                'type'       => 'text',
            )
        );

        //Social Facebook
        $cmb_social->add_field(
            array(
                'name'       => __('Facebook Text', 'burgercompanhiacombr'),
                'desc'       => '',
                'id'         => $prefix . 'social_facebook_text',
                'type'       => 'textarea_code',
            )
        );

        $cmb_social->add_field(
            array(
                'name'       => __('Facebook URL', 'burgercompanhiacombr'),
                'desc'       => '',
                'id'         => $prefix . 'social_facebook_url',
                'type'       => 'text_url',
            )
        );

        //Social Instagram
        $cmb_social->add_field(
            array(
                'name'       => __('Instagram Text', 'burgercompanhiacombr'),
                'desc'       => '',
                'id'         => $prefix . 'social_instagram_text',
                'type'       => 'textarea_code',
            )
        );

        $cmb_social->add_field(
            array(
                'name'       => __('Instagram URL', 'burgercompanhiacombr'),
                'desc'       => '',
                'id'         => $prefix . 'social_instagram_url',
                'type'       => 'text_url',
            )
        );
    }
);
